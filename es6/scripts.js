import isEqualDeep from './isEqualDeep.js';

window.addEventListener('load', function(){
    let obj = {here: {is: "an"}, object: [1,3]};
    //console.log(isEqualDeep(obj, obj));        
    // -> true
    //console.log(isEqualDeep(obj, {here12: 1, object: 2}));
    // -> false
    //console.log(isEqualDeep(obj, {here: {is: "an"}, object: 3}));
    // -> true
    //console.log(isEqualDeep(obj, {here: {is: "an"}, object: [1,3]}));
    //console.log(isEqualDeep(arr, ['angel', 'clown', 'mandarin', {a:1,b:2}]));

    console.log(isEqualDeep(new Date, null));



});