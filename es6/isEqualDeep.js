export default function isEqualDeep(a = {}, b = {}) {
    if (a === b) {  //смотрит на ячейку памяти
        return true;
    }

    if (a == null || typeof(a) != "object" || b == null || typeof(b) != "object") {
        return false;
    }    
    
    if ( Object.keys(a).length !== Object.keys(b).length ) {    //сравниваю длины
        return false;
    }

    for(let prop in a) {
        if ( !b.hasOwnProperty(prop) ) {    //ищу свойства в другом объекте
            return false;
        }

        if ( (typeof a[prop] === 'object' && typeof b[prop] !== 'object') || 
             (typeof a[prop] !== 'object' && typeof b[prop] === 'object') ) {   //проверка на object
            return false;
        } 
        
        if ( typeof a[prop] === 'object' && typeof b[prop] === 'object' ) { //если оба object, то рекурсия
            if ( !isEqualDeep(a[prop], b[prop]) ) {
                return false;
            }
        } else if ( a[prop] !== b[prop] ) { //финальная проверка
            return false;
        }
    }

    return true;
    
}